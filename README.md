# [My Personal Website][site]

- Powered by [Hugo][ssg]
- Themed with [Codex][theme]
- Deployed by [GitLab CI][ci]
- Hosted on [GitLab Pages][pages]

---

[![pipeline status][badge]][piplines]

[site]: https://liolok.com
[ssg]: https://gohugo.io
[theme]: https://github.com/liolok/hugo-theme-codex
[ci]: https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/
[pages]: https://about.gitlab.com/stages-devops-lifecycle/pages/
[badge]: https://gitlab.com/liolok/liolok.com/badges/master/pipeline.svg
[piplines]: https://gitlab.com/liolok/liolok.com/-/pipelines
